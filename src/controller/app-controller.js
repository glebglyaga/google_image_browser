import * as ActionType from '../actions/action-type';
import AppView from '../view/app-view';
import {clearContainer} from '../utils/dom-utils';
import Logger from '../utils/logger';
import {fetchImages} from '../service/net-service';
import Model from '../model/model';

const AppController = config => {
    const logger = Logger.createTaggedLoggers('AppController');
    const model = Model();

    const addListeners = () => {
        model.addObserver(ActionType.SEARCH_QUERY_DID_CHANGE, data => search(data.text));
    };

    const dispose = () => {
        model.dispose();
        clearContainer(config.container);
    };

    const search = query => {
        if (DEBUG) {
            logger.log('Searching for:', query);
        }

        fetchImages(query)
            .then(result => {
            if (DEBUG) {
                logger.log('fetchImages() result: ', result);
            }

            model.setSearchResult(result);
        });
    };

    if (DEBUG) {
        logger.log('App initialized with config:', config);
    }

    config.container.appendChild(AppView(model));

    addListeners();

    return {
        dispose
    };
};

export default AppController;