import AppController from './controller/app-controller';

export function Main(config) {
    const appController = AppController(config);

    this.dispose = () => appController.dispose();
}

require('../style/style.scss');