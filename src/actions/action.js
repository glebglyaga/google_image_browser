import * as ActionType from './action-type';

export const searchQuery = query => {
    return {
        type: ActionType.SEARCH_QUERY_DID_CHANGE,
        data: query
    };
};

export const searchResult = result => {
    return {
        type: ActionType.SEARCH_RESULT_DID_CHANGE,
        data: result
    };
};

export const selectedImage = image => {
    return {
        type: ActionType.SELECTED_IMAGE_DID_CHANGE,
        data: image
    };
};