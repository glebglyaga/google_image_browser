export const SEARCH_QUERY_DID_CHANGE = 'searchQueryDidChange';
export const SEARCH_RESULT_DID_CHANGE = 'searchResultDidChange';
export const SELECTED_IMAGE_DID_CHANGE = 'selectedImageDidChange';