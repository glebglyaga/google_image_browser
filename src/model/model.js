import {selectedImage, searchQuery, searchResult} from '../actions/action';
import ActionBus from '../utils/action-bus';

const getInitialState = () => {
    return {
        images       : [],
        selectedImage: null,
        observers    : {},
        query        : null,
    };
};

const Model = () => {
    const actionBus = new ActionBus();
    const state = getInitialState();

    const addObserver = (type, callback) => actionBus.addObserver(type, callback);

    const dispose = () => {
        actionBus.dispose();
    };

    const setNextImage = () => {
        const nextImage = state.images[state.selectedImage.index + 1];

        if (nextImage !== undefined) {
            setSelectedImage(nextImage);
        }
    };

    const setSearchResult = images => {
        state.images = images;
        actionBus.dispatch(searchResult(images));
    };

    const setSelectedImage = image => {
        state.selectedImage = image;
        actionBus.dispatch(selectedImage(image));
    };

    const setPrevImage = () => {
        const prevImage = state.images[state.selectedImage.index - 1];

        if (prevImage !== undefined) {
            setSelectedImage(prevImage);
        }
    };

    const setQuery = query => {
        state.query = query;
        actionBus.dispatch(searchQuery(query));
    };

    return {
        addObserver, dispose, setNextImage, setSearchResult, setSelectedImage, setPrevImage, setQuery
    };
};

export default Model;