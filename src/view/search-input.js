import * as ClassName from '../model/class-names';
import {makeElement} from '../utils/dom-utils';

const SearchInput = model => {
    const inputKeyDownHandler = e => {
        if (e.keyCode === 13) {
            model.setQuery({text: e.target.value});
        }
    };

    const view = makeElement('div', {className: ClassName.SEARCH_INPUT},
        makeElement('input', {
            type     : 'text',
            onkeydown: inputKeyDownHandler
        })
    );

    return view;
};


export default SearchInput;