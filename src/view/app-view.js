import * as ClassName from '../model/class-names';
import {makeElement} from '../utils/dom-utils';
import LightBoxView from './light-box';
import SearchInput from './search-input';
import SearchResultView from './search-result-view';

const AppView = model => {
    const view = makeElement('div', {className: ClassName.APP_CONTAINER}, [
        SearchInput(model),
        SearchResultView(model),
        LightBoxView(model)
    ]);

    return view;
};

export default AppView;