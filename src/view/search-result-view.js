import * as ActionType from '../actions/action-type';
import * as ClassName from '../model/class-names';
import {makeElement, swapNodes} from '../utils/dom-utils';

const SearchResultView = model => {
    let imageContainer = makeElement('div', {className: ClassName.IMAGES_CONTAINER});
    const view = makeElement('div', {className: ClassName.SEARCH_RESULT}, imageContainer);

    const addListeners = () => {
        model.addObserver(ActionType.SEARCH_RESULT_DID_CHANGE, update);
    };

    const update = items => {
        if (items) {
            let newImageContainer = makeElement('div', {className: ClassName.IMAGES_CONTAINER}, items.map(item => {
                return makeElement('div', {className: ClassName.IMAGES_CONTAINER_ITEM}, makeElement('img', {
                    src: item.thumbnailUrl,
                    title: item.title,
                    onclick: () => model.setSelectedImage(item),
                    width: item.thumbnailWidth,
                    height: item.thumbnailHeight
                }));
            }));

            swapNodes(imageContainer, newImageContainer);
            imageContainer = newImageContainer;
        }
    };

    addListeners();

    return view;
};

export default SearchResultView;