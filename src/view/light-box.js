import * as ActionTypes from '../actions/action-type';
import {makeElement, swapNodes} from '../utils/dom-utils';
import Logger from '../utils/logger';
import * as ClassName from '../model/class-names';
import {NEXT, PREV, SPINNER} from './assets';

const LightBoxView = model => {
    let content = makeElement('div', {className: ClassName.LIGHT_BOX_CONTENT});
    const image = new Image();
    const logger = Logger.createTaggedLoggers('LightBoxView');
    const spinner = makeElement('div', {className: ClassName.SPINNER_LAYER, innerHTML: SPINNER});
    const view = makeElement('div', {className: ClassName.LIGHT_BOX},
        makeElement('div', {className: 'wrapper'}, [
            makeElement('div', {
                className: ClassName.LIGHT_BOX_PREV,
                innerHTML: PREV,
                onclick: () => model.setPrevImage()
            }),
            content,
            makeElement('div', {
                className: ClassName.LIGHT_BOX_NEXT,
                innerHTML: NEXT,
                onclick: () => model.setNextImage()
            })
        ])
    );

    const addListeners = () => {
        model.addObserver(ActionTypes.SELECTED_IMAGE_DID_CHANGE, update);
    };

    const update = data => {
        logger.log('LightBox data:', data);

        if (data !== null) {
            const title = makeElement('div', {className: ClassName.LIGHT_BOX_TITLE, text: data.title,});

            let newContent = makeElement('div', {
                className: ClassName.LIGHT_BOX_CONTENT,
                onclick: () => model.setSelectedImage(null)
            }, [spinner, title]);

            image.src = data.imageUrl;
            image.onload = () => {
                swapNodes(spinner, makeElement('div', {className: ClassName.IMAGE_CONTAINER}, image));
                title.style.visibility = 'visible';
            };
            title.style.visibility = 'hidden';

            swapNodes(content, newContent);
            content = newContent;

            view.style.display = 'block';
        } else {
            view.style.display = 'none';
        }
    };

    addListeners();

    return view;
};

export default LightBoxView;