export function appendArray(el, children) {
    children.forEach(child => {
        if (Array.isArray(child)) {
            appendArray(el, child);
        } else if (child instanceof window.Element) {
            el.appendChild(child);
        }
    });
}

export function clearContainer(element) {
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

export function makeElement(type, props, ...otherChildren) {
    const el = window.document.createElement(type);

    if (typeof props === 'object') {
        Object.keys(props).forEach(prop => {
            if (prop in el) {
                el[prop] = props[prop];
            } else if (typeof props.text === 'string') {
                el.appendChild(document.createTextNode(props.text));
            } else {
                console.warn(`${prop} is not a valid property of a <${type}>`, props);
            }
        });
    }

    if (otherChildren) {
        appendArray(el, otherChildren);
    }

    return el;
}

export function swapNodes(oldNode, newNode) {
    if (oldNode.parentElement) {
        oldNode.parentElement.replaceChild(newNode, oldNode);
    }

    return newNode;
}