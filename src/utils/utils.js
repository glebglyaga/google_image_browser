export function leadingZero(value, length) {
    value = value.toString();

    while (value.length < length) {
        value = '0' + value;
    }
    return value;
}