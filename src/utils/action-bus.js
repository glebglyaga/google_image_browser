export default class ActionBus {
    constructor() {
        this.observers = {};
    }

    addObserver(type, callback) {
        if (this.observers[type] !== undefined) {
            this.observers[type].push({callback});
        } else {
            this.observers[type] = [{callback}];
        }
    }

    dispatch({type, data}) {
        if (this.observers[type] !== undefined) {
            const observers = this.observers[type].slice();
            const observersLen = observers.length;
            let i = 0;

            if (this.observers[type] !== undefined) {
                for (i; i < observersLen; i++) {
                    observers[i].callback(data);
                }
            }
        }
    }

    dispose() {
        this.observers = {};
    }

    removeObserver(type, callback) {
        if (this.observers[type] !== undefined) {
            const observersLen = this.observers[type].length;
            let i = 0, observer;

            for (i; i < observersLen; i++) {
                observer = this.observers[type][i];

                if (observer.callback === callback) {
                    this.observers[type].splice(i, 1);
                }
            }
        }
    }
}