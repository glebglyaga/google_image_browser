import * as Constant from '../model/constant';
import Logger from '../utils/logger';

export const fetchImages = (query, page = 1) => {
    return fetch(`${Constant.API_URL}?key=${Constant.API_KEY}&cx=${Constant.CX}&q=${query}&fields=items(image,link,title),queries&client=google-csbe&searchType=image&start=${1 + page * 10}`)
    // return fetch('http://localhost:5050/response.json')
        .then(response => response.json())
        .then(response => {
            const result = {
                items: response.items
            };

            if (DEBUG) {
                Logger.createTaggedLogger('fetchImages')('response: ', response);
            }

            return result.items.map((item, index) => {
                return {
                    index,
                    imageUrl       : item.link,
                    thumbnailHeight: item.image.thumbnailHeight,
                    thumbnailUrl   : item.image.thumbnailLink,
                    thumbnailWidth : item.image.thumbnailWidth,
                    title          : item.title,
                };
            });
        });
};