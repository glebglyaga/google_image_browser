var BrowserSyncPlugin = require('browser-sync-webpack-plugin'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    path              = require('path'),
    webpack           = require('webpack');

var packageJson = require('./package.json');

function getDefinePlugin(environment, version) {
    var config = {
        DEBUG       : JSON.stringify(JSON.parse(environment.DEBUG || 'true')),
        VERSION     : JSON.stringify(version),
        BUILD       : JSON.stringify(JSON.parse(environment.BUILD || Date.now() * 0.001)),
        PRODUCT_NAME: JSON.stringify('Google Image Browser')
    };

    return new webpack.DefinePlugin(config);
}

module.exports = {
    context: path.resolve(__dirname, 'src'),
    entry  : './index.js',
    output : {
        filename: 'google_image_browser.js',
        library : 'GoogleImageBrowser',
        path    : path.resolve(__dirname, 'dist')
    },

    plugins: [
        new BrowserSyncPlugin({
            host  : 'localhost',
            port  : '5050',
            open  : false,
            server: {
                baseDir: ['www', 'dist']
            }
        }),

        getDefinePlugin(process.env, packageJson.version),

        new ExtractTextPlugin({
            filename: 'style.css',
            allChunks: true
        }),

        // new webpack.ProvidePlugin({
        //     Promise: "imports-loader?this=>global!exports-loader?global.Promise!es6-promise",
        //     fetch: "imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch",
        // })
    ],
    module: {
        rules: [{
            test   : /\.js$/,
            include: [
                path.resolve(__dirname, 'src')
            ],
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader',
                options: { presets: ['es2015'] }
            }]
        }, {
            test: /\.scss$/,
            include: [
                path.resolve(__dirname, 'style')
            ],
            exclude: /node_modules/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',

                // Could also be write as follow:
                // use: 'css-loader?modules&importLoader=2&sourceMap&localIdentName=[local]!sass-loader'
                use: [
                    {
                        loader: 'css-loader',
                        query: {
                            modules: true,
                            sourceMap: true,
                            importLoaders: 2,
                            localIdentName: '[local]'
                        }
                    },
                    'sass-loader'
                ]
            })
        }]
    }
};
