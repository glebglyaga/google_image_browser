import {expect} from 'chai';

import {appendArray, clearContainer, makeElement} from '../src/utils/dom-utils';

describe('Dom Utils', () => {

    describe('Append Array', () => {
        let container;

        it ('Appends array of elements', () => {
            container = makeElement('div', {});

            appendArray(container, [
                makeElement('div'),
                makeElement('div'),
                makeElement('div'),
                makeElement('div'),
            ]);
            expect(container.childElementCount).to.be.equal(4);
        });
    });

    describe('Clear Container', () => {
        let container;

        it ('Clears container', () => {
            container = makeElement('div', {}, [
                makeElement('ul', {}, [
                    makeElement('li'),
                    makeElement('li'),
                    makeElement('li'),
                    makeElement('li'),
                ]),
                makeElement('div'),
                makeElement('div'),
                makeElement('div'),
                makeElement('div'),
            ]);

            clearContainer(container);
            expect(container.childElementCount).to.be.equal(0);
        });
    });

    describe('Make Element', () => {
        it ('Creates elements with properties', () => {
            let element = makeElement('div', {className: 'className'});
            expect(element.className).to.be.equal('className');
        });

        it ('Creates nested elements', () => {
            let element = makeElement('div', {}, [
                makeElement('ul', {}, [
                    makeElement('li'),
                    makeElement('li'),
                    makeElement('li'),
                    makeElement('li'),
                ]),
                makeElement('div'),
                makeElement('div'),
                makeElement('div'),
                makeElement('div'),
            ]);
            expect(element.childElementCount).to.be.equal(5);
        })
    });
});
