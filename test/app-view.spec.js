import {expect} from 'chai';

import ActionBus from '../src/utils/action-bus';
import AppView from '../src/view/app-view';
import * as ClassName from '../src/model/class-names';

describe('App View', () => {
    let view;

    beforeEach(() => {
        view = AppView(new ActionBus());
    });

    it ('Creates Search Input', () => {
        let input = view.querySelector('input');
        expect(input).to.exist;
    });

    it ('Creates Images Container', () => {
        let imagesContainer = view.querySelector(`.${ClassName.IMAGES_CONTAINER}`);
        expect(imagesContainer).to.exist;
    });

    it ('Creates Light Box', () => {
        let input = view.querySelector('input');
        expect(input).to.exist;
    });
});
