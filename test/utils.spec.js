import {expect} from 'chai';

import {leadingZero} from '../src/utils/utils';

describe('Utils', () => {
    describe('Leading zero', () => {
        it('adds leading zeros', () => {
            expect(leadingZero(8, 2)).to.be.equal('08');
            expect(leadingZero(8, 4)).to.be.equal('0008');
        });

        it('doesn`t add leading zero', () => {
            expect(leadingZero(28, 2)).to.be.equal('28');
            expect(leadingZero(270, 3)).to.be.equal('270');
        });
    });
});
