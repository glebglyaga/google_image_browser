import {expect} from 'chai';
import Sinon from 'sinon';

import ActionBus from '../src/utils/action-bus';

describe('Action Bus', () => {
    const ACTION = 'someAction';
    let actionBus;
    let listenerSpy;

    beforeEach(() => {
        actionBus = new ActionBus();
        listenerSpy = Sinon.spy();
    });

    it('adds listeners', () => {
        let x = () => undefined;

        actionBus.addObserver(ACTION, listenerSpy);
        actionBus.addObserver(ACTION, x);

        expect(actionBus.observers[ACTION]).to.have.length(2);
    });

    it('removes listeners', () => {
        let x = () => undefined;

        actionBus.addObserver(ACTION, listenerSpy);
        actionBus.addObserver(ACTION, x);
        actionBus.removeObserver(ACTION, x);

        expect(actionBus.observers[ACTION]).to.have.length(1);
    });

    it('invokes listener with arguments', () => {
        actionBus.addObserver(ACTION, listenerSpy);
        actionBus.dispatch({type: ACTION, data: {text: 'someText'}});

        expect(listenerSpy.calledWith({text: 'someText'})).to.be.true;
    });

    it('invokes all listener', () => {
        let anotherSpy = Sinon.spy();

        actionBus.addObserver(ACTION, listenerSpy);
        actionBus.addObserver(ACTION, anotherSpy);
        actionBus.dispatch({type: ACTION});

        expect(listenerSpy.calledOnce).to.be.true;
        expect(anotherSpy.calledOnce).to.be.true;
    });

});
